export const login = '/login';
export const courses = '/courses';
export const registration = '/registration';
export const courseAdd = '/courses/add';
export const courseId = '/courses/:courseId';
