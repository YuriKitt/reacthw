export const getCourses = (state) => state.courses.courses;
export const getAuthors = (state) => state.authors.authors;
export const getUsers = (state) => state.user;
export const isAuthenticated = (state) => state.user.isAuth;
export const userName = (state) => state.user.name;
export const userEmail = (state) => state.user.email;
export const userToken = (state) => state.user.token;
