import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { Link } from 'react-router-dom';

import { Button } from '../../../../common';
import { pipeDuration } from '../../../../helpers';

import * as url from '../../../../urls';
import * as coursesActions from '../../../../store/courses/actionCreators';

import './CourseCard.css';

export const CourseCard = ({
	id,
	title,
	duration,
	creationDate,
	description,
	authors,
}) => {
	const dispatch = useDispatch();

	const authorNames = authors.join(', ');

	const handleEdit = () => {
		console.log('edit');
	};

	const handleDelete = () => {
		dispatch(coursesActions.deleteCourse(id));
	};

	return (
		<div className='course-card'>
			<div className='course-card_title'>
				<h2>{title}</h2>
				<p>{description}</p>
			</div>
			<div className='course-card_info'>
				<p className='course-card_author'>
					<strong>Authors:</strong> {authorNames}
				</p>
				<p>
					<strong>Created:</strong> {creationDate}
				</p>
				<p>
					<strong>Duration:</strong> {pipeDuration(duration)}
				</p>
				<div className='course-card_buttons'>
					<Link to={`${url.courses}/${id}`} className='course-card_link'>
						<Button buttonText='Show Course' className='course-card_button' />
					</Link>
					<Button
						buttonText='E'
						className='course-card_button__small'
						onClick={handleEdit}
					/>
					<Button
						buttonText='D'
						className='course-card_button__small'
						onClick={handleDelete}
					/>
				</div>
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	id: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	duration: PropTypes.number.isRequired,
	creationDate: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired,
	authors: PropTypes.arrayOf(PropTypes.string).isRequired,
};
