import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { Input, Button } from '../../../../common';

import './SearchBar.css';

export const SearchBar = ({ setSearchTerm }) => {
	const [input, setInput] = useState('');
	const handleInputChange = (event) => {
		setInput(event.target.value);
		if (!event.target.value) {
			setSearchTerm('');
		}
	};
	const handleButtonClick = () => {
		setSearchTerm(input);
	};
	const handleKeyDown = (event) => {
		if (event.key === 'Enter') {
			setSearchTerm(input);
		}
	};
	return (
		<div className='search-bar'>
			<Input
				id='search-input'
				placeholderText='Enter course name or id...'
				onChange={handleInputChange}
				onKeyDown={handleKeyDown}
				type='text'
				labelText=''
				value={input}
			/>
			<Button
				buttonText='Search'
				onClick={handleButtonClick}
				className='search-button'
			/>
		</div>
	);
};

SearchBar.propTypes = {
	setSearchTerm: PropTypes.func.isRequired,
};

export default SearchBar;
