import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { CourseCard, SearchBar } from './components';
import { Button } from '../../common';
import { getAuthorNames } from '../../helpers';
import * as selector from '../../selectors';
import * as url from '../../urls';

import './Courses.css';

export const Courses = () => {
	const coursesList = useSelector(selector.getCourses);
	const authorsList = useSelector(selector.getAuthors);

	const [searchTerm, setSearchTerm] = useState('');

	const filteredCourses = coursesList.filter(
		(course) =>
			course.title.toLowerCase().includes(searchTerm.toLowerCase()) ||
			course.id.toLowerCase().includes(searchTerm.toLowerCase())
	);

	return (
		<div className='courses'>
			<div className='courses_control'>
				<SearchBar setSearchTerm={setSearchTerm} />
				<Link to={url.courseAdd} className='courses_add-new-link'>
					<Button
						buttonText='Add new course'
						className='courses_add-new-button'
					/>
				</Link>
			</div>
			{filteredCourses.map((course) => {
				return (
					<CourseCard
						{...course}
						authors={getAuthorNames(course, authorsList)}
						key={course.id}
					/>
				);
			})}
		</div>
	);
};
