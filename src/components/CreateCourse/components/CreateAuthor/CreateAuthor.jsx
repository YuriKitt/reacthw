import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import PropTypes from 'prop-types';

import { Input, Button } from '../../../../common';
import * as authorsActions from '../../../../store/authors/actionCreators';

import './CreateAuthor.css';

export const CreateAuthor = ({ setAuthors }) => {
	const dispatch = useDispatch();
	const [newAuthorName, setNewAuthorName] = useState('');

	const handleNewAuthorNameChange = (event) =>
		setNewAuthorName(event.target.value);

	const handleCreateAuthor = () => {
		if (newAuthorName.trim().length < 2) {
			alert('too short Author name ');
			return;
		}

		const newAuthor = {
			name: newAuthorName,
			id: uuidv4(),
		};

		dispatch(authorsActions.addAuthor(newAuthor));

		setAuthors((prevState) => {
			return [...prevState, newAuthor].sort((a, b) =>
				a.name.localeCompare(b.name)
			);
		});
		setNewAuthorName('');
	};

	return (
		<div className='create-author'>
			<h3>Add author</h3>
			<Input
				id='authorName'
				type='text'
				labelText='Author name:'
				placeholderText='Enter author name...'
				value={newAuthorName}
				onChange={handleNewAuthorNameChange}
			/>
			<Button
				buttonText='Create author'
				onClick={handleCreateAuthor}
				className='create-author_button'
			/>
		</div>
	);
};

CreateAuthor.propTypes = {
	setAuthors: PropTypes.func.isRequired,
};
