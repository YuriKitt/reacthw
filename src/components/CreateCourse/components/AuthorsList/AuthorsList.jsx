import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '../../../../common';

import './AuthorsList.css';

export const AuthorsList = ({
	authors,
	setAuthors,
	courseAuthors,
	setCourseAuthors,
}) => {
	const handleAddAuthor = (author) => {
		setAuthors((prevState) => prevState.filter((a) => a.id !== author.id));
		setCourseAuthors((prevState) => [...prevState, author]);
	};

	const handleDeleteAuthor = (author) => {
		setCourseAuthors((prevState) =>
			prevState.filter((a) => a.id !== author.id)
		);
		setAuthors((prevState) => {
			return [...prevState, author].sort((a, b) =>
				a.name.localeCompare(b.name)
			);
		});
	};

	return (
		<div className='authors-adding'>
			<div className='authors-adding_list'>
				<h3>Authors</h3>
				<ul>
					{authors.map((author) => (
						<li key={author.id} className='authors-adding_list-item'>
							<p className='authors-adding_author-name'>{author.name}</p>
							<Button
								buttonText='Add author'
								className='authors-adding_button'
								onClick={() => handleAddAuthor(author)}
							/>
						</li>
					))}
				</ul>
			</div>
			<div className='authors-adding_course-list'>
				<h3>Course authors</h3>
				{courseAuthors.length === 0 ? (
					<p className='authors-adding_empty-course'>Author list is empty.</p>
				) : (
					<ul>
						{courseAuthors.map((author) => (
							<li key={author.id} className='authors-adding_course-list-item'>
								<p className='authors-adding_author-name'>{author.name}</p>
								<Button
									buttonText='Delete author'
									className='authors-adding_button'
									onClick={() => handleDeleteAuthor(author)}
								></Button>
							</li>
						))}
					</ul>
				)}
			</div>
		</div>
	);
};

AuthorsList.propTypes = {
	authors: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
		})
	).isRequired,
	setAuthors: PropTypes.func.isRequired,
	courseAuthors: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
		})
	).isRequired,
	setCourseAuthors: PropTypes.func.isRequired,
};
