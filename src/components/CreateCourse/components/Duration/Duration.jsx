import React from 'react';
import PropTypes from 'prop-types';

import { Input } from '../../../../common';
import { pipeDuration } from '../../../../helpers';

import './Duration.css';

export const Duration = ({ duration, setDuration }) => {
	const handleDurationChange = (event) => {
		let valValue = event.target.value;
		if (valValue < 0) {
			valValue = 0;
		}
		valValue = Math.floor(valValue);
		setDuration(valValue);
		event.target.value = valValue || '';
	};
	return (
		<div className='duration'>
			<h3>Duration</h3>
			<Input
				id='duration'
				type='number'
				labelText='Duration:'
				placeholderText='Enter duration in minutes...'
				value={duration || ''}
				onChange={handleDurationChange}
			/>
			<p className='duration_hours'>Duration: {pipeDuration(duration)}</p>
		</div>
	);
};

Duration.propTypes = {
	duration: PropTypes.number.isRequired,
	setDuration: PropTypes.func.isRequired,
};
