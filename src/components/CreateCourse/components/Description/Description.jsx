import React from 'react';

import PropTypes from 'prop-types';

import './Description.css';

export const Description = ({ description, setDescription }) => {
	const handleDescriptionChange = (e) => setDescription(e.target.value);

	return (
		<div className='description'>
			<label htmlFor='courseDescription'>Description:</label>
			<textarea
				className='description_textarea'
				id='courseDescription'
				value={description}
				onChange={handleDescriptionChange}
				placeholder='Enter description...'
			/>
		</div>
	);
};

Description.propTypes = {
	description: PropTypes.string.isRequired,
	setDescription: PropTypes.func.isRequired,
};
