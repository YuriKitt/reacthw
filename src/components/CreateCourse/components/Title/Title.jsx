import React from 'react';

import PropTypes from 'prop-types';

import { Input, Button } from '../../../../common';

import './Title.css';

export const Title = ({ title, setTitle, handleCreateCourse }) => {
	const handleTitleChange = (event) => setTitle(event.target.value);

	return (
		<div className='title'>
			<Input
				id='Title'
				type='text'
				labelText='Title:'
				placeholderText='Enter title...'
				value={title}
				onChange={handleTitleChange}
			/>
			<Button
				buttonText='Create Course'
				onClick={handleCreateCourse}
				className='title_create-course-button'
			/>
		</div>
	);
};

Title.propTypes = {
	title: PropTypes.string.isRequired,
	setTitle: PropTypes.func.isRequired,
	handleCreateCourse: PropTypes.func.isRequired,
};
