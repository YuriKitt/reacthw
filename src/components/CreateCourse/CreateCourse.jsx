import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useNavigate, Link } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';

import { dateGenerator } from '../../helpers';

import * as selector from '../../selectors';
import * as coursesActions from '../../store/courses/actionCreators';
import * as url from '../../urls';

import {
	Title,
	Description,
	CreateAuthor,
	Duration,
	AuthorsList,
} from './components';

import './CreateCourse.css';

export const CreateCourse = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const authorsList = useSelector(selector.getAuthors);

	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState(0);
	const [authors, setAuthors] = useState(
		[...authorsList].sort((a, b) => a.name.localeCompare(b.name))
	);
	const [courseAuthors, setCourseAuthors] = useState([]);

	const handleCreateCourse = () => {
		if (!title || !description || !courseAuthors.length || duration === 0) {
			alert('All fields are required.');
			return;
		}
		if (title.trim().length < 2) {
			alert('Title must be at least 2 characters.');
			return;
		}
		if (description.trim().length < 2) {
			alert('Description must be at least 2 characters.');
			return;
		}

		const newCourse = {
			title,
			description,
			creationDate: dateGenerator(),
			duration,
			authors: courseAuthors.map((a) => a.id),
			id: uuidv4(),
		};
		dispatch(coursesActions.addCourse(newCourse));

		navigate(url.courses);
	};

	return (
		<div className='create-course'>
			<Link className='link' to={url.courses}>
				{'< '}Back to courses
			</Link>
			<Title
				title={title}
				setTitle={setTitle}
				handleCreateCourse={handleCreateCourse}
			/>
			<Description description={description} setDescription={setDescription} />
			<div className='create-course_authors'>
				<div className='create-course_authors__left'>
					<CreateAuthor setAuthors={setAuthors} />
					<Duration duration={duration} setDuration={setDuration} />
				</div>
				<AuthorsList
					authors={authors}
					setAuthors={setAuthors}
					courseAuthors={courseAuthors}
					setCourseAuthors={setCourseAuthors}
				/>
			</div>
		</div>
	);
};
