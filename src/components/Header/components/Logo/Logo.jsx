import React from 'react';

import logo from './Logo.png';
import './Logo.css';

export const Logo = () => <img src={logo} alt='Logo' className='logo' />;
