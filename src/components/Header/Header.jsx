import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { Logo } from './components';
import { Button } from '../../common';
import * as selector from '../../selectors';
import * as userActions from '../../store/user/actionCreators';
import * as url from '../../urls';

import './Header.css';

export const Header = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const isAuthenticated = useSelector(selector.isAuthenticated);
	const userName = useSelector(selector.userName);

	const handleLogout = () => {
		// localStorage.removeItem('token');
		localStorage.clear();
		dispatch(userActions.logout());
		navigate(url.login);
	};
	return (
		<header className='header'>
			<div className='header_logo'>
				<Logo />
			</div>
			{isAuthenticated && (
				<>
					<h2>Welcome, {userName}</h2>
					<Button
						buttonText='Logout'
						onClick={handleLogout}
						className='header_logout-button'
					/>
				</>
			)}
		</header>
	);
};
