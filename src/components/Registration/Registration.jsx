import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';

import { Input, Button } from '../../common';
import * as url from '../../urls';

import './Registration.css';

export const Registration = () => {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [notification, setNotification] = useState({ message: '', type: '' });

	const navigate = useNavigate();

	const handleSubmit = async (event) => {
		event.preventDefault();

		const newUser = {
			name,
			password,
			email,
		};

		try {
			const response = await fetch('http://localhost:4000/register', {
				method: 'POST',
				body: JSON.stringify(newUser),
				headers: {
					'Content-Type': 'application/json',
				},
			});

			const result = await response.json();

			if (response.ok) {
				setNotification({ message: result.result, type: 'success' });
				setTimeout(() => {
					navigate(url.login);
				}, 1000);
			} else {
				const errorsString = result.errors.join('\n');
				setNotification({ message: errorsString, type: 'error' });
			}
		} catch (error) {
			setNotification({ message: 'Error: ' + error.message, type: 'error' });
		}
	};

	return (
		<div className='registration'>
			<div className='registration_container'>
				<h3>Registration</h3>
				<form onSubmit={handleSubmit} className='registration_form'>
					<Input
						id='registration-name'
						type='text'
						labelText='Name'
						placeholderText='Enter Name'
						value={name}
						onChange={(e) => setName(e.target.value)}
					/>
					<Input
						id='registration-email'
						type='email'
						labelText='Email'
						placeholderText='Enter Email'
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
					<Input
						id='registration-password'
						type='password'
						labelText='Password'
						placeholderText='Enter Password'
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
					<div className='registration_button-container'>
						<Button
							type='submit'
							buttonText='Regisatration'
							className='registration_button'
						/>
					</div>
				</form>
				<p>
					Already have an account?{' '}
					<Link className='registration_link' to={url.login}>
						Login
					</Link>
				</p>
			</div>
			{notification.message && (
				<div className={`notification ${notification.type}`}>
					{notification.message}
				</div>
			)}
		</div>
	);
};
