import React from 'react';
import { useSelector } from 'react-redux';

import { useParams, Link, Navigate } from 'react-router-dom';

import { getAuthorNames, pipeDuration } from '../../helpers';
import * as selector from '../../selectors';
import * as url from '../../urls';

import './CourseInfo.css';

export const CourseInfo = () => {
	const coursesList = useSelector(selector.getCourses);
	const authorsList = useSelector(selector.getAuthors);

	const { courseId } = useParams();
	const course = coursesList.find((course) => course.id === courseId);
	const { title, description, id, duration, creationDate } = course || {};

	return course ? (
		<div className='course-info'>
			<Link className='course-info_link' to={url.courses}>
				{'< '}Back to courses
			</Link>
			<h3>{title}</h3>
			<div className='course-info_description-container'>
				<div className='course-info_description'>
					<p>{description}</p>
				</div>
				<div className='course-info_info'>
					<p className='course-info_id'>
						<b>ID:</b> {id}
					</p>
					<p>
						<b>Duration:</b> {pipeDuration(duration)}
					</p>
					<p>
						<b>Created</b>: {creationDate}
					</p>
					<p className='course-info_authors'>
						<b>Authors:</b>
						<br />
						{getAuthorNames(course, authorsList).join('\n')}
					</p>
				</div>
			</div>
		</div>
	) : (
		<Navigate to={url.courses} />
	);
};
