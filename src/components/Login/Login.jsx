import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { Link } from 'react-router-dom';

import { Input, Button } from '../../common';
import * as userActions from '../../store/user/actionCreators';
import * as url from '../../urls';

import './Login.css';

export const Login = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const handleLogin = (result) => {
		localStorage.setItem('token', result.result);
		localStorage.setItem('name', result.user.name);
		dispatch(userActions.login(result));
		navigate(url.courses);
	};

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [notification, setNotification] = useState({ message: '', type: '' });

	const handleSubmit = async (event) => {
		event.preventDefault();

		const userCredentials = {
			password,
			email,
		};

		try {
			const response = await fetch('http://localhost:4000/login', {
				method: 'POST',
				body: JSON.stringify(userCredentials),
				headers: {
					'Content-Type': 'application/json',
				},
			});

			const result = await response.json();

			if (response.ok) {
				setNotification({
					message: `Welcome! ${result.user.name}`,
					type: 'success',
				});
				setTimeout(() => {
					handleLogin(result);
				}, 1000);
			} else {
				const errorsString = result.errors.join('\n');
				setNotification({ message: errorsString, type: 'error' });
			}
		} catch (error) {
			setNotification({ message: 'Error: ' + error.message, type: 'error' });
		}
	};

	return (
		<div className='login'>
			<div className='login_container'>
				<h3>Login</h3>
				<form onSubmit={handleSubmit} className='login_form'>
					<Input
						id='login-email'
						type='email'
						labelText='Email'
						placeholderText='Enter Email'
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
					<Input
						id='login-password'
						type='password'
						labelText='Password'
						placeholderText='Enter Password'
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
					<div className='login_button-container'>
						<Button type='submit' buttonText='Login' className='login_button' />
					</div>
				</form>
				<p>
					Don't have an account?{' '}
					<Link className='login_link' to={url.registration}>
						Register
					</Link>
				</p>
			</div>
			{notification.message && (
				<div className={`notification ${notification.type}`}>
					{notification.message}
				</div>
			)}
		</div>
	);
};
