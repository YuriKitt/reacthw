import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import PropTypes from 'prop-types';

import * as selector from './selectors';

import {
	Courses,
	CreateCourse,
	Registration,
	Login,
	CourseInfo,
} from './components';

import * as url from './urls';

const ProtectedRoute = ({
	isAuthenticated,
	redirectPath = url.login,
	children,
}) => {
	if (!isAuthenticated) {
		return <Navigate to={redirectPath} />;
	}
	return children;
};

const Router = () => {
	const isAuthenticated = useSelector(selector.isAuthenticated);
	return (
		<Routes>
			<Route
				index
				element={
					<ProtectedRoute isAuthenticated={isAuthenticated}>
						<Navigate to={url.courses} />
					</ProtectedRoute>
				}
			/>
			<Route
				path={url.courses}
				element={
					<ProtectedRoute isAuthenticated={isAuthenticated}>
						<Courses />
					</ProtectedRoute>
				}
			/>
			<Route
				path={url.courseAdd}
				element={
					<ProtectedRoute isAuthenticated={isAuthenticated}>
						<CreateCourse />
					</ProtectedRoute>
				}
			/>
			<Route
				path={url.courseId}
				element={
					<ProtectedRoute isAuthenticated={isAuthenticated}>
						<CourseInfo />
					</ProtectedRoute>
				}
			/>
			<Route
				path={url.registration}
				element={
					<ProtectedRoute
						isAuthenticated={!isAuthenticated}
						redirectPath={url.courses}
					>
						<Registration />
					</ProtectedRoute>
				}
			/>
			<Route
				path={url.login}
				element={
					<ProtectedRoute
						isAuthenticated={!isAuthenticated}
						redirectPath={url.courses}
					>
						<Login />
					</ProtectedRoute>
				}
			/>
			<Route
				path='*'
				element={
					<ProtectedRoute isAuthenticated={isAuthenticated}>
						<Navigate to={url.courses} />
					</ProtectedRoute>
				}
			/>
		</Routes>
	);
};

ProtectedRoute.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired,
	redirectPath: PropTypes.string,
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
};

export default Router;
