export const getAuthorNames = (course, authorsList) => {
	return authorsList
		.filter((author) => course.authors.includes(author.id))
		.map((author) => author.name);
};
