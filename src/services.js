const API_BASE_URL = 'http://localhost:4000';

const handleResponse = async (response) => {
	if (!response.ok) {
		const errorData = await response.json();
		const errorMessage = errorData.message || 'Something went wrong';
		throw new Error(errorMessage);
	}
	return response.json();
};

export const getAllCourses = async () => {
	const response = await fetch(`${API_BASE_URL}/courses/all`);
	return handleResponse(response);
};

export const getAllAuthors = async () => {
	const response = await fetch(`${API_BASE_URL}/authors/all`);
	return handleResponse(response);
};
