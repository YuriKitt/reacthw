import React from 'react';
import PropTypes from 'prop-types';

import './Input.css';

export const Input = ({
	id,
	type,
	labelText,
	placeholderText,
	onChange,
	onKeyDown,
	value,
}) => {
	return (
		<div className='input'>
			<label htmlFor={id}>{labelText}</label>
			<input
				id={id}
				type={type}
				placeholder={placeholderText}
				onChange={onChange}
				onKeyDown={onKeyDown}
				value={value}
			/>
		</div>
	);
};

Input.propTypes = {
	id: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	labelText: PropTypes.string.isRequired,
	placeholderText: PropTypes.string,
	onChange: PropTypes.func.isRequired,
	onKeyDown: PropTypes.func,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
