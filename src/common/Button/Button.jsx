import React from 'react';
import PropTypes from 'prop-types';

import './Button.css';

export const Button = ({ buttonText, className, ...restProps }) => {
	return (
		<button className={className} {...restProps}>
			{buttonText}
		</button>
	);
};

Button.propTypes = {
	buttonText: PropTypes.string.isRequired,
	className: PropTypes.string.isRequired,
};
