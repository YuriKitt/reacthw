import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { Header } from './components';
import Router from './Router';

import * as coursesActions from './store/courses/actionCreators';
import * as authorsActions from './store/authors/actionCreators';
import * as userActions from './store/user/actionCreators';
import { getAllAuthors, getAllCourses } from './services';

import './App.css';

function App() {
	const dispatch = useDispatch();

	useEffect(() => {
		!!localStorage.getItem('token') && dispatch(userActions.active());

		getAllAuthors().then((result) => {
			dispatch(authorsActions.fetchAuthors(result.result));
		});

		getAllCourses().then((result) => {
			dispatch(coursesActions.fetchCourses(result.result));
		});
	}, [dispatch]);

	return (
		<div className='App'>
			<Header />
			<Router />
		</div>
	);
}

export default App;
