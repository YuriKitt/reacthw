import * as actionTypes from './actionTypes';

export const login = (result) => ({
	type: actionTypes.LOGIN,
	payload: {
		isAuth: true,
		name: result.user.name,
		email: result.user.email,
		token: result.result,
	},
});

export const logout = () => ({
	type: actionTypes.LOGOUT,
});

export const active = () => ({
	type: actionTypes.ACTIVE,
	payload: {
		isAuth: true,
		name: localStorage.getItem('name'),
		email: '',
		token: localStorage.getItem('token'),
	},
});
