import * as actionTypes from './actionTypes';

const initialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
};

function reducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.LOGIN:
			return {
				...action.payload,
			};
		case actionTypes.LOGOUT:
			return {
				...initialState,
			};
		case actionTypes.ACTIVE:
			return {
				...action.payload,
			};

		default:
			return state;
	}
}

export default reducer;
