import * as actionTypes from './actionTypes';

const initialState = {
	authors: [],
};

function reducer(state = initialState, action) {
	switch (action.type) {
		case actionTypes.FETCH_AUTHORS:
			return { ...state, authors: action.payload.authors };

		case actionTypes.ADD_AUTHOR:
			return { ...state, authors: [...state.authors, action.payload.author] };

		default:
			return state;
	}
}

export default reducer;
