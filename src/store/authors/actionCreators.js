import * as actionTypes from './actionTypes';

export const fetchAuthors = (authors) => ({
	type: actionTypes.FETCH_AUTHORS,
	payload: { authors },
});

export function addAuthor(author) {
	return {
		type: actionTypes.ADD_AUTHOR,
		payload: { author },
	};
}
