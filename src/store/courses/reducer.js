import * as actionTypes from './actionTypes';

const initialState = {
	courses: [],
};

const courseReducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.FETCH_COURSES:
			return { ...state, courses: action.payload.courses };

		case actionTypes.DELETE_COURSE:
			return {
				...state,
				courses: state.courses.filter(
					(course) => course.id !== action.payload.id
				),
			};

		case actionTypes.ADD_COURSE:
			return { ...state, courses: [...state.courses, action.payload.course] };

		default:
			return state;
	}
};

export default courseReducer;
