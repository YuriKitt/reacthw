import * as actionTypes from './actionTypes';

export const fetchCourses = (courses) => ({
	type: actionTypes.FETCH_COURSES,
	payload: { courses },
});

export const deleteCourse = (id) => ({
	type: actionTypes.DELETE_COURSE,
	payload: { id },
});

export function addCourse(course) {
	return {
		type: actionTypes.ADD_COURSE,
		payload: { course },
	};
}
